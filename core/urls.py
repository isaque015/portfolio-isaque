from django.contrib import admin
from django.urls import path
from core.views import view_index


urlpatterns = [
    path('', view_index),
    path('index/', view_index, name = 'index'),
]
