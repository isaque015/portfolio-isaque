from django.shortcuts import render
from django.http import request


def view_index(request):
    return render(request, 'index.html')

# Create your views here.
